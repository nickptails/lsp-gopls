ARG ALPINE_VERSION
ARG GOLANG_VERSION

FROM golang:${GOLANG_VERSION}-alpine${ALPINE_VERSION} AS builder

ARG GOPLS_VERSION

RUN apk add --no-cache \
        build-base \
        curl \
        tar && \
    curl -fLo gopls-${GOPLS_VERSION}.tar.gz \
        https://github.com/golang/tools/archive/gopls/v${GOPLS_VERSION}.tar.gz && \
    tar xzf gopls-${GOPLS_VERSION}.tar.gz && \
    cd tools-gopls-v${GOPLS_VERSION}/gopls && \
    CGO_ENABLED=0 go build -trimpath -ldflags="-s -w" -v -o /go/bin/gopls


FROM golang:${GOLANG_VERSION}-alpine${ALPINE_VERSION}

COPY --from=builder /go/bin/ /usr/local/bin/
RUN apk add --no-cache \
        nmap-ncat && \
    chmod +x /usr/local/bin/gopls

CMD ["gopls"]
